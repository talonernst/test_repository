﻿namespace TEAssignment3
{
    partial class frmGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlGameBoard = new System.Windows.Forms.Panel();
            this.picBox9 = new System.Windows.Forms.PictureBox();
            this.picBox8 = new System.Windows.Forms.PictureBox();
            this.picBox7 = new System.Windows.Forms.PictureBox();
            this.picBox6 = new System.Windows.Forms.PictureBox();
            this.picBox5 = new System.Windows.Forms.PictureBox();
            this.picBox4 = new System.Windows.Forms.PictureBox();
            this.picBox3 = new System.Windows.Forms.PictureBox();
            this.picBox2 = new System.Windows.Forms.PictureBox();
            this.picBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblWhosTurn = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rtbWinners = new System.Windows.Forms.RichTextBox();
            this.pnlGameBoard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlGameBoard
            // 
            this.pnlGameBoard.BackColor = System.Drawing.Color.White;
            this.pnlGameBoard.Controls.Add(this.picBox9);
            this.pnlGameBoard.Controls.Add(this.picBox8);
            this.pnlGameBoard.Controls.Add(this.picBox7);
            this.pnlGameBoard.Controls.Add(this.picBox6);
            this.pnlGameBoard.Controls.Add(this.picBox5);
            this.pnlGameBoard.Controls.Add(this.picBox4);
            this.pnlGameBoard.Controls.Add(this.picBox3);
            this.pnlGameBoard.Controls.Add(this.picBox2);
            this.pnlGameBoard.Controls.Add(this.picBox1);
            this.pnlGameBoard.Location = new System.Drawing.Point(313, 87);
            this.pnlGameBoard.Name = "pnlGameBoard";
            this.pnlGameBoard.Size = new System.Drawing.Size(506, 519);
            this.pnlGameBoard.TabIndex = 0;
            // 
            // picBox9
            // 
            this.picBox9.Location = new System.Drawing.Point(339, 347);
            this.picBox9.Name = "picBox9";
            this.picBox9.Size = new System.Drawing.Size(160, 165);
            this.picBox9.TabIndex = 8;
            this.picBox9.TabStop = false;
            this.picBox9.Click += new System.EventHandler(this.picBox9_Click);
            // 
            // picBox8
            // 
            this.picBox8.Location = new System.Drawing.Point(173, 347);
            this.picBox8.Name = "picBox8";
            this.picBox8.Size = new System.Drawing.Size(160, 165);
            this.picBox8.TabIndex = 7;
            this.picBox8.TabStop = false;
            this.picBox8.Click += new System.EventHandler(this.picBox8_Click);
            // 
            // picBox7
            // 
            this.picBox7.Location = new System.Drawing.Point(7, 347);
            this.picBox7.Name = "picBox7";
            this.picBox7.Size = new System.Drawing.Size(160, 165);
            this.picBox7.TabIndex = 6;
            this.picBox7.TabStop = false;
            this.picBox7.Click += new System.EventHandler(this.picBox7_Click);
            // 
            // picBox6
            // 
            this.picBox6.Location = new System.Drawing.Point(339, 176);
            this.picBox6.Name = "picBox6";
            this.picBox6.Size = new System.Drawing.Size(160, 165);
            this.picBox6.TabIndex = 5;
            this.picBox6.TabStop = false;
            this.picBox6.Click += new System.EventHandler(this.picBox6_Click);
            // 
            // picBox5
            // 
            this.picBox5.Location = new System.Drawing.Point(173, 176);
            this.picBox5.Name = "picBox5";
            this.picBox5.Size = new System.Drawing.Size(160, 165);
            this.picBox5.TabIndex = 4;
            this.picBox5.TabStop = false;
            this.picBox5.Click += new System.EventHandler(this.picBox5_Click);
            // 
            // picBox4
            // 
            this.picBox4.Location = new System.Drawing.Point(7, 176);
            this.picBox4.Name = "picBox4";
            this.picBox4.Size = new System.Drawing.Size(160, 165);
            this.picBox4.TabIndex = 3;
            this.picBox4.TabStop = false;
            this.picBox4.Click += new System.EventHandler(this.picBox4_Click);
            // 
            // picBox3
            // 
            this.picBox3.Location = new System.Drawing.Point(339, 5);
            this.picBox3.Name = "picBox3";
            this.picBox3.Size = new System.Drawing.Size(160, 165);
            this.picBox3.TabIndex = 2;
            this.picBox3.TabStop = false;
            this.picBox3.Click += new System.EventHandler(this.picBox3_Click);
            // 
            // picBox2
            // 
            this.picBox2.Location = new System.Drawing.Point(173, 5);
            this.picBox2.Name = "picBox2";
            this.picBox2.Size = new System.Drawing.Size(160, 165);
            this.picBox2.TabIndex = 1;
            this.picBox2.TabStop = false;
            this.picBox2.Click += new System.EventHandler(this.picBox2_Click);
            // 
            // picBox1
            // 
            this.picBox1.Location = new System.Drawing.Point(7, 5);
            this.picBox1.Name = "picBox1";
            this.picBox1.Size = new System.Drawing.Size(160, 165);
            this.picBox1.TabIndex = 0;
            this.picBox1.TabStop = false;
            this.picBox1.Click += new System.EventHandler(this.picBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Black", 26F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(389, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(365, 73);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tic Tac Toe";
            // 
            // lblWhosTurn
            // 
            this.lblWhosTurn.AutoSize = true;
            this.lblWhosTurn.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWhosTurn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblWhosTurn.Location = new System.Drawing.Point(825, 85);
            this.lblWhosTurn.Name = "lblWhosTurn";
            this.lblWhosTurn.Size = new System.Drawing.Size(245, 37);
            this.lblWhosTurn.TabIndex = 2;
            this.lblWhosTurn.Text = "Currently X\'s Turn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(5, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 37);
            this.label2.TabIndex = 4;
            this.label2.Text = "Game Winners";
            // 
            // rtbWinners
            // 
            this.rtbWinners.ForeColor = System.Drawing.SystemColors.MenuText;
            this.rtbWinners.Location = new System.Drawing.Point(12, 125);
            this.rtbWinners.Name = "rtbWinners";
            this.rtbWinners.ReadOnly = true;
            this.rtbWinners.Size = new System.Drawing.Size(178, 351);
            this.rtbWinners.TabIndex = 5;
            this.rtbWinners.Text = "";
            // 
            // frmGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Crimson;
            this.ClientSize = new System.Drawing.Size(1078, 617);
            this.Controls.Add(this.rtbWinners);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblWhosTurn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlGameBoard);
            this.Name = "frmGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tic Tac Toe";
            this.Load += new System.EventHandler(this.FrmGame_Load);
            this.pnlGameBoard.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlGameBoard;
        private System.Windows.Forms.PictureBox picBox9;
        private System.Windows.Forms.PictureBox picBox8;
        private System.Windows.Forms.PictureBox picBox7;
        private System.Windows.Forms.PictureBox picBox6;
        private System.Windows.Forms.PictureBox picBox5;
        private System.Windows.Forms.PictureBox picBox4;
        private System.Windows.Forms.PictureBox picBox3;
        private System.Windows.Forms.PictureBox picBox2;
        private System.Windows.Forms.PictureBox picBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblWhosTurn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtbWinners;
    }
}

